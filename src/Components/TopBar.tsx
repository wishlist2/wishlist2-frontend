import React from 'react';

class TopBar extends React.Component<{}, {}> {
  public render(): JSX.Element {
    return (
      <div className="top-bar">
        <span>Wish List</span>
      </div>
    );
  }
}

export default TopBar;
