import React from 'react';
import logo from './logo.svg';
import TopBar from './Components/TopBar'
import './App.css';

class App extends React.Component<{}, {}> {
  public render(): JSX.Element {
    return (
      <div className="App">
        <TopBar />
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </div>
    );
  }
}

export default App;
